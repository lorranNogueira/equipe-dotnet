import userService from "../app/service/userService";

export default function ItensMenu() {


  //constroi o menu com base nos recusos do ususario da sessão
  const montaMenu = () => {
    const itens = [];


      itens.push({
        _tag: "CSidebarNavItem",
        name: "Dashboard",
        to: "/dashboard",
        icon: "cil-speedometer",
      });

    itens.push({
      _tag: "CSidebarNavItem",
      name: "Usuários",
      to: "/usuario/lista",
      icon: "cil-user",
    });

    itens.push({
      _tag: "CSidebarNavItem",
      name: "Lista de Tarefas",
      to: "/listaTarefas/lista",
      icon: "cil-user",
    });

    //constroi itens menu Administração

      //const menuAdmin = {
      //  _tag: "CSidebarNavDropdown",
      //  name: "Administração",
      //  route: "/administracao",
      //  icon: "cil-user",
      //  _children: [],
      //};


      //      menuAdmin._children.push({
      //        _tag: "CSidebarNavItem",
      //        name: "Configurações",
      //        to: "/administracao/configuracao",
      //      });
          
      //      menuAdmin._children.push({
      //        _tag: "CSidebarNavItem",
      //        name: "Quartos",
      //        to: "/quarto/lista",
      //      });
          
      //      menuAdmin._children.push({
      //        _tag: "CSidebarNavItem",
      //        name: "Usuários",
      //        to: "/usuario/lista",
      //      });
          

//      itens.push(menuAdmin);
    

    return itens;
  };

  return montaMenu();
}
