import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import { useAuth } from './provedorAutenticacao'
import { Alert } from "./components/Alert";
import './scss/style.scss';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));


function RotaAutenticada( { component: Component, isAutenticado, ...props } ){
  return (
      <Route {...props} render={ (componentProps) => {
          if(isAutenticado){
              return (
                  <Component {...componentProps} />
              )
          }else{
              return(
                <>
                  <Alert/>
                  <Redirect to={ {pathname : '/login', state : { from: componentProps.location } } } />
                </>
              )
          }
      }}  />
  )
}

const App = () => {
  const { isAutenticado } = useAuth();
    return (
        <HashRouter>
            <React.Suspense fallback={loading}>
            <Switch>                
                <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
                <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                <RotaAutenticada path="/" isAutenticado={isAutenticado} name="Home" component={TheLayout} />
            </Switch>
          </React.Suspense>
        </HashRouter>
    );
  }

  export default App;
