
function itemsMenu(nomeMenu){
  const usuario = JSON.parse(localStorage.getItem("USER"));
  return usuario.recursos.filter((element) => element.nomeMenu === nomeMenu);;
};

function temRecurso(nome){
  const usuario = JSON.parse(localStorage.getItem("USER"));
  return usuario.recursos.some(r => r.nome === nome);
};

function recursos(){
  const usuario = JSON.parse(localStorage.getItem("USER"));
  return usuario.recursos.map(r => r.nome);
};

function obterHashAutenticacao(nomeMenu) {
  const usuario = JSON.parse(localStorage.getItem("USER"));
  if (usuario) {
    return usuario.jwtToken;
  }
  return "";
}

export default {
 // itemsMenu,
 // obterHashAutenticacao,
//  temRecurso,
 // recursos
};
