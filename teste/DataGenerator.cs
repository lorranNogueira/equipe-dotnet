﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListProject.Infra.Data.Context;
using TodoListProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace TodoListProject.Web
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DataBaseContext(serviceProvider.GetRequiredService<DbContextOptions<DataBaseContext>>()))
            {
                var usuario1 = new Usuario
                       {
                           Nome = "lorran",
                           Email = "lorrannogueirafontes@gmail.com",
                           Senha = "123"
                       };

                var usuario2 = new Usuario
                {
                    Nome = "admin",
                    Email = "admin@gmail.com",
                    Senha = "123"
                };


                var categoria1 = new Categoria() { Nome = "Categoria 1" };
                var categoria2 = new Categoria() { Nome = "Categoria 2" };
                var categoria3 = new Categoria() { Nome = "Categoria 3" };

                var lista = new ListaTarefas()
                {
                    Nome = "Lista 1",
                    Categoria = categoria1,
                };

                var tarefa1 = new Tarefa()
                {
                    Nome = "tarefa 1",
                    Usuario = usuario1,
                    ListaTarefas = lista
                };

                var tarefa2 = new Tarefa()
                {
                    Nome = "tarefa 2",
                    Usuario = usuario1,
                    ListaTarefas = lista
                };

                var tarefa3 = new Tarefa()
                {
                    Nome = "tarefa 3",
                    Usuario = usuario1,
                    ListaTarefas = lista
                };

                var tarefa4 = new Tarefa()
                {
                    Nome = "tarefa 4",
                    Usuario = usuario1,
                    ListaTarefas = lista
                };

                var tarefa5 = new Tarefa()
                {
                    Nome = "tarefa 5",
                    Usuario = usuario1,
                    ListaTarefas = lista
                };

                lista.Tarefas = new List<Tarefa>();
                lista.Tarefas.Add(tarefa1);
                lista.Tarefas.Add(tarefa2);


                context.Usuario.Add(usuario1);
                context.Usuario.Add(usuario2);

                context.Categoria.Add(categoria1);
                context.Categoria.Add(categoria2);
                context.Categoria.Add(categoria3);

                context.ListaTarefas.Add(lista);

                context.Tarefa.Add(tarefa1);
                context.Tarefa.Add(tarefa2);
                context.Tarefa.Add(tarefa3);
                context.Tarefa.Add(tarefa4);
                context.Tarefa.Add(tarefa5);

                context.SaveChanges();
            }
        }
    }
}
