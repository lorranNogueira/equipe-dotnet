﻿

using System.Collections.Generic;
using TodoListProject.Application;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Services;

namespace TodoListProject.Application

{
    public class ListaTarefasAppService : AppServiceBase<ListaTarefas>, IListaTarefasAppService
    {
        private readonly IListaTarefasService _listaTarefasService;

        public ListaTarefasAppService(IListaTarefasService listaTarefasService)
            : base(listaTarefasService)
        {
            _listaTarefasService = listaTarefasService;
        }

        public ListaTarefas GetListaTarefa(int listaTarefaId)
        {
            return _listaTarefasService.GetListaTarefa(listaTarefaId);
        }

        public IEnumerable<ListaTarefas> GetAllListaTarefa()
        {
            return _listaTarefasService.GetAllListaTarefa();
        }
    }
}
