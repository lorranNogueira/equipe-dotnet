﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        Usuario GetByNomeAndSenha(string nome, string senha);
    }
}
