import ApiService from '../apiService'

const listar = () => {
  return ApiService.get('Tarefa')
};

const obter = (id) => {
  return ApiService.get("Tarefa/" + id)
};

const salvar = (tarefa) => {
  return ApiService.post('Tarefa', tarefa)
};

const remover = (id) => {
  return ApiService.remove('Tarefa/' + id)
};

const update = (id, tarefa) => {
  return ApiService.update('Tarefa/' + id, tarefa)
};

export default {
  listar,
  salvar,
  obter,
  remover,
  update
};
