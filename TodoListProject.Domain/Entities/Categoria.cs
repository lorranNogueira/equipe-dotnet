﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoListProject.Domain.Entities
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }
    }
}
