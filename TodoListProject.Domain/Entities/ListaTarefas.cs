﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TodoListProject.Domain.Entities
{
    public class ListaTarefas
    {
        public int ListaTarefasId { get; set; }
        public string Nome { get; set; }
        public int CategoriaId { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual ICollection<Tarefa> Tarefas { get; set; }
        public bool Concluida { get; set; }
    }
}
