﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListProject.Api.ViewModels;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListaTarefasController : ControllerBase
    {
        private readonly IListaTarefasAppService _listaTarefasAppService;
        private IMapper _mapper;

        public ListaTarefasController(IListaTarefasAppService listaTarefasAppService, IMapper mapper)
        {
            _listaTarefasAppService = listaTarefasAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var listaTarefas = _listaTarefasAppService.GetAllListaTarefa();

            var listaTarefasViewer = _mapper.Map<IEnumerable<ListaTarefasViewModel>>(listaTarefas);

            return Ok(listaTarefasViewer);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var listaTarefas = _listaTarefasAppService.GetListaTarefa(id);

            var listaTarefasViewer = _mapper.Map<ListaTarefasViewModel>(listaTarefas);

            return Ok(listaTarefasViewer);
        }

        [HttpPost]
        public void Post(ListaTarefasViewModel listaTarefasViewModel)
        {
            var listaTarefasDominio = _mapper.Map<ListaTarefas>(listaTarefasViewModel);

            _listaTarefasAppService.Add(listaTarefasDominio);
        }

        [HttpPut("{id}")]
        public void Put(int id, ListaTarefasViewModel listaTarefasViewModel)
        {
            var ListaTarefa = _listaTarefasAppService.GetById(id);

            ListaTarefa.Concluida = listaTarefasViewModel.Concluida;

            _listaTarefasAppService.Update(ListaTarefa);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var listaTarefas = _listaTarefasAppService.GetById(id);
            _listaTarefasAppService.Remove(listaTarefas);
        }
    }
}
