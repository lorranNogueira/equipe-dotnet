﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Domain.Interfaces.Services
{
    public interface IListaTarefasService : IServiceBase<ListaTarefas>
    {
        ListaTarefas GetListaTarefa(int listaTarefaId);
        IEnumerable<ListaTarefas> GetAllListaTarefa();
    }
}
