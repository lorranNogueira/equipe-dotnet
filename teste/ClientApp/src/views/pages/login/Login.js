import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import ApiService from "../../../app/apiService";
import { UsuarioContext } from "../../../provedorAutenticacao";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import { Alert } from "../../../components/Alert";

const Login = () => {
  const [login, setLogin] = useState("");
  const [senha, setSenha] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);

  const usuarioContexto = useContext(UsuarioContext);

  let history = useHistory();

  const entrar = async () => {
    setIsSubmitting(true);
    ApiService.post("/Authentication", { nome: login, senha: senha })
      .then((r) => {
        usuarioContexto.iniciarSessao(r.data);
        history.push("/");
      })
      .catch(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="5">
            <CCard className="p-4">
              <Alert />
              <CCardBody>
                <CForm>
                  <h1>Login</h1>
                  
                  <p style={{ color: 'red' }}>Nome: admin / Senha: 123</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon content={freeSet.cilUser} />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={login}
                      onChange={(e) => setLogin(e.target.value)}
                      type="text"
                      placeholder="Nome"
                      autoComplete="username"
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon content={freeSet.cilLockLocked} />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={senha}
                      onChange={(e) => setSenha(e.target.value)}
                      type="password"
                      placeholder="Senha"
                      autoComplete="current-password"
                    />
                  </CInputGroup>
                  <CRow>
                    <CCol xs="6">
                      <CButton
                        onClick={entrar}
                        color="primary"
                        className="px-4"
                        disabled={isSubmitting}
                      >
                        {isSubmitting ? "Entrando..." : "Entrar"}
                      </CButton>
                    </CCol>
                    <CCol xs="6" className="text-right">
                      <CButton color="link" className="px-0">
                        Esqueceu a senha?
                      </CButton>
                    </CCol>
                  </CRow>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
