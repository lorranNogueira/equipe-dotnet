﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListProject.Api.ViewModels;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarefaController : ControllerBase
    {
        private readonly ITarefaAppService _tarefaAppService;
        private IMapper _mapper;

        public TarefaController(ITarefaAppService tarefaAppService, IMapper mapper)
        {
            _tarefaAppService = tarefaAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var listaTarefa = _tarefaAppService.GetAll();

            var tarefaViewer = _mapper.Map<IEnumerable<TarefaViewModel>>(listaTarefa);

            return Ok(tarefaViewer);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var tarefa = _tarefaAppService.GetById(id);

            var tarefaViewer = _mapper.Map<TarefaViewModel>(tarefa);

            return Ok(tarefaViewer);
        }

        [HttpPost]
        public void Post(TarefaViewModel tarefaViewModel)
        {
            var tarefaDominio = _mapper.Map<Tarefa>(tarefaViewModel);

            _tarefaAppService.Add(tarefaDominio);
        }

        [HttpPut("{id}")]
        public void Put(int id, TarefaViewModel tarefaViewModel)
        {
            var tarefa = _tarefaAppService.GetById(id);

            tarefa.Concluida = tarefaViewModel.Concluida;

            if (!string.IsNullOrWhiteSpace(tarefaViewModel.Nome))
            {
                tarefa.Nome = tarefaViewModel.Nome;
            }
            
            _tarefaAppService.Update(tarefa);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var tarefa = _tarefaAppService.GetById(id);
            _tarefaAppService.Remove(tarefa);
        }
    }
}
