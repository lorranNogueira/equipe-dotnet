﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TodoListProject.Api.ViewModels
{
    public class TarefaViewModel
    {
        [Key]
        public int TarefaId { get; set; }
        public int ListaTarefasId { get; set; }
        public long UsuarioId { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
        public string Nome { get; set; }
        public bool Concluida { get; set; }
    }
}
