using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using TodoListProject.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Infra.Data.Repositories;
using TodoListProject.Domain.Entities;
using AutoMapper;
using TodoListProject.Api.ViewModels;
using TodoListProject.Application.Interface;
using TodoListProject.Application;
using TodoListProject.Domain.Interfaces.Services;
using TodoListProject.Domain.Services;

namespace TodoListProject.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataBaseContext>(opt => opt.UseInMemoryDatabase(databaseName: "TodoList"));

            services.AddScoped<DataBaseContext, DataBaseContext>();

            services.AddTransient<IUsuarioAppService, UsuarioAppService>();
            services.AddTransient<IListaTarefasAppService, ListaTarefasAppService>();
            services.AddTransient<ITarefaAppService, TarefaAppService>();

            services.AddTransient<IUsuarioService, UsuarioService>();
            services.AddTransient<IListaTarefasService, ListaTarefasService>();
            services.AddTransient<ITarefaService, TarefaService>();

            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IListaTarefasRepository, ListaTarefasRepository>();
            services.AddTransient<ITarefaRepository, TarefaRepository>();

            AutoMapperConfig(services);

            services.AddControllersWithViews();

            services.AddControllers().AddJsonOptions(option => { option.JsonSerializerOptions.PropertyNamingPolicy = null; option.JsonSerializerOptions.MaxDepth = 100000; option.JsonSerializerOptions.IgnoreNullValues = true; });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

        private void AutoMapperConfig(IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Usuario, UsuarioViewModel>();
                cfg.CreateMap<UsuarioViewModel, Usuario>();

                cfg.CreateMap<ListaTarefas, ListaTarefasViewModel>();
                cfg.CreateMap<ListaTarefasViewModel, ListaTarefas>();

                cfg.CreateMap<Tarefa, TarefaViewModel>();
                cfg.CreateMap<TarefaViewModel, Tarefa>();

                cfg.CreateMap<Categoria, CategoriaViewModel>();
                cfg.CreateMap<CategoriaViewModel, Categoria>();
            });

            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
