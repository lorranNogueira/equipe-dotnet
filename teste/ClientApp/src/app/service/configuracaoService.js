import ApiService from '../apiService'

const urlConfiguracao = 'configuracoes/edita';

function obter() {
    return ApiService.get(urlConfiguracao);
}

function salvar(configuracao) {
    return ApiService.post(urlConfiguracao, configuracao);
}

export const configuracaoService = {
    salvar,
    obter
};