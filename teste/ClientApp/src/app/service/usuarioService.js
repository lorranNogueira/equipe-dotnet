import ApiService from '../apiService'

const autenticar = (credenciais) => {
  return ApiService.post('/api/authenticate', credenciais)
};

const listarUsuarios = () => {
  return ApiService.get('usuario')
};

const obterUsuario = (id) => {
  return ApiService.get("usuario/" + id)
};

//const editarUsuario = (id, usuario) => {
//  return ApiService.post('usuario/editar/' + id, usuario)
//};

const salvarUsuario = (usuario) => {
  return ApiService.post('usuario', usuario)
};

const removerUsuario = (id) => {
  return ApiService.remove('usuario/' + id)
};

export default {
  autenticar,
  listarUsuarios,
  salvarUsuario,
  obterUsuario,
  removerUsuario
};
