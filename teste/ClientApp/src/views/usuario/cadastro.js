import React, { useState } from "react";
import UsuarioService from "../../app/service/usuarioService";
import {
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CCol,
  CForm,
  CInvalidFeedback,
  CFormGroup,
  CLabel,
  CInput,
  CRow,
  CTextarea,
} from "@coreui/react";
import Swal from "sweetalert2";
import { Formik } from "formik";
import * as Yup from "yup";

const validationSchema = function (values) {
  return Yup.object().shape({
    nome: Yup.string().required("Nome é obrigatório"),
    email: Yup.string().email("Email invalido").required("Email é obrigatório"),
    senha: Yup.string().required("Senha é obrigatório"),
  });
};

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values);
    try {
      validationSchema.validateSync(values, { abortEarly: false });
      return {};
    } catch (error) {
      return getErrorsFromValidationError(error);
    }
  };
};

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0;
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    };
  }, {});
};

const ConfiguracoesForms = () => {
  const [configuracoes, setconfiguracoes] = useState({
    nome: "",
    email: "",
    senha: ""
  });

  const onSubmit = async (values, { setSubmitting, setErrors }) => {
    setSubmitting(true);
    const usuario = {
      nome: values.nome,
      email: values.email,
      senha: values.senha,
    };

    UsuarioService.salvarUsuario(usuario)
      .then((r) => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Usuário salvo',
          showConfirmButton: false,
          timer: 3000
        })


        setSubmitting(false);
      }).catch((erro) => {
        console.log(erro);
      });

  };

  return (
    <CCard>
      <CCardBody>
        <Formik
          initialValues={configuracoes}
          validate={validate(validationSchema)}
          onSubmit={onSubmit}
          enableReinitialize={true}
        >
          {({
            values,
            errors,
            touched,
            status,
            dirty,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched,
          }) => (
            <CForm onSubmit={handleSubmit} noValidate name="form">
              <CRow>
                <CCol lg="12">
                  <CCardHeader>Cadastro de usuário</CCardHeader>
                  <br />
                  <CRow>
                  <CCol md={4}>
                    <CFormGroup>
                      <CLabel htmlFor="nome">Nome</CLabel>
                      <CInput
                        maxLength={30}
                        type="text"
                        name="nome"
                        id="nome"
                        placeholder="Nome"
                        autoComplete="nome"
                        valid={!errors.nome}
                        invalid={touched.nome && !!errors.nome}
                        autoFocus={true}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.nome}
                      />
                      <CInvalidFeedback>{errors.nome}</CInvalidFeedback>
                    </CFormGroup>
                  </CCol>
                  <CCol md={4}>
                    <CFormGroup>
                      <CLabel htmlFor="email">Email</CLabel>
                      <CInput
                        maxLength={50}
                        type="email"
                        name="email"
                        id="email"
                        placeholder="Email"
                        autoComplete="email"
                        valid={!errors.email}
                        invalid={touched.email && !!errors.email}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                      />
                      <CInvalidFeedback>{errors.email}</CInvalidFeedback>
                    </CFormGroup>
                  </CCol>
                  <CCol md={4}>
                    <CFormGroup>
                      <CLabel htmlFor="senha">Senha</CLabel>
                      <CInput
                        maxLength={50}
                        type="password"
                        name="senha"
                        id="senha"
                        placeholder="senha"
                        autoComplete="senha"
                        valid={!errors.senha}
                        invalid={touched.senha && !!errors.senha}
                        required
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.senha}
                      />
                      <CInvalidFeedback>{errors.senha}</CInvalidFeedback>
                    </CFormGroup>
                    </CCol>
                  </CRow>
                </CCol>

                <CCol>
                  <br />
                  <CFormGroup>
                    <CButton
                      type="submit"
                      color="success"
                      className="mr-1"
                      disabled={isSubmitting}
                    >
                      {isSubmitting ? "Salvando..." : "Salvar"}
                    </CButton>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CForm>
          )}
        </Formik>
      </CCardBody>
    </CCard>
  );
};

export default ConfiguracoesForms;
