﻿

using System.Collections.Generic;
using TodoListProject.Application;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Services;

namespace TodoListProject.Application
{
    public class UsuarioAppService : AppServiceBase<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService)
            : base(usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public Usuario GetByNomeAndSenha(string nome, string senha)
        {
            return _usuarioService.GetByNomeAndSenha(nome, senha);
        }

    }
}
