﻿
using System.Collections.Generic;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Application.Interface
{
    public interface ITarefaAppService : IAppServiceBase<Tarefa>
    {
        
    }
}
