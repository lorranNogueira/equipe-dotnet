import ApiService from '../apiService'

const listar = () => {
  return ApiService.get('ListaTarefas')
};

const obter = (id) => {
  return ApiService.get("ListaTarefas/" + id)
};

const salvar = (listaTarefas) => {
  return ApiService.post('ListaTarefas', listaTarefas)
};

const remover = (id) => {
  return ApiService.remove('ListaTarefas/' + id)
};

const update = (id, listaTarefas) => {
  return ApiService.update('ListaTarefas/' + id, listaTarefas)
};

export default {
  listar,
  salvar,
  obter,
  remover,
  update
};
