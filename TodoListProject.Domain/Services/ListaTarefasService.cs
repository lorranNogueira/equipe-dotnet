﻿

using System.Collections.Generic;
using System.Linq;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Domain.Interfaces.Services;
using TodoListProject.Domain.Services;

namespace TodoListProject.Domain.Services
{
    public class ListaTarefasService : ServiceBase<ListaTarefas>, IListaTarefasService
    {
        private readonly IListaTarefasRepository _listaTarefasRepository;

        public ListaTarefasService(IListaTarefasRepository listaTarefasRepository)
            : base(listaTarefasRepository)
        {
            _listaTarefasRepository = listaTarefasRepository;
        }

        public ListaTarefas GetListaTarefa(int listaTarefaId)
        {
            return _listaTarefasRepository.GetListaTarefa(listaTarefaId);
        }

        public IEnumerable<ListaTarefas> GetAllListaTarefa()
        {
            return _listaTarefasRepository.GetAllListaTarefa();
        }

    }
}
