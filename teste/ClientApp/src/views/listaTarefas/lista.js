import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  CCard,
  CCardHeader,
  CCol,
  CRow,
  CCardBody,
  CBadge,
  CButton,
  CButtonGroup,
  CDataTable,
  CLabel,
  CInput,
  CInputCheckbox,
  CContainer,
  CFormGroup,
  CCollapse,
  CSelect
} from "@coreui/react";
import { BsEye, BsTrash, BsGrid3X3, BsPlusCircle } from "react-icons/bs";
import { FaRegEdit } from "react-icons/fa";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import ListaTarefasService from "../../app/service/listaTarefasService";
import TarefaService from "../../app/service/tarefaService";

const modal = withReactContent(Swal);

const Tabela = () => {
  useEffect(() => {
    ListaTarefasService.listar().then((r) => {
      setItems(r.data);
      setloading(false);
    });
  }, []);

  const [details, setDetails] = useState([])
  const [items, setItems] = useState();
  const [loading, setloading] = useState(true);
  let history = useHistory();

  const excluir = (id) => {
    Swal.fire({
      title: "Excluir?",
      text: "Não vai ser possível desfazer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Sim, excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        ListaTarefasService.remover(id).then((r) => {
          Swal.fire({
            title: "Lista excluída!",
            timer: 2000,
            timerProgressBar: true,
            icon: "success",
          }).then(
            ListaTarefasService.listar().then((r) => {
              setItems(r.data);
              setloading(false);
            })
          );
        });
      }
    });
  };

  const excluirTarefa = (id) => {
    Swal.fire({
      title: "Excluir?",
      text: "Não vai ser possível desfazer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Sim, excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        TarefaService.remover(id).then((r) => {
          Swal.fire({
            title: "Tarefa excluída!",
            timer: 2000,
            timerProgressBar: true,
            icon: "success",
          }).then(
            ListaTarefasService.listar().then((r) => {
              setItems(r.data);
              setloading(false);
            })
          );
        });
      }
    });
  };

  const concluirTarefa = (tarefaId, concluida) => {

    const tarefa = {
      TarefaId: tarefaId, Concluida: concluida
    };
    // const titulo = (concluida : "Tarefa marcada como concluida!" ? "Tarefa marcada como não concluida!")

    TarefaService.update(tarefaId, tarefa).then((r) => {
      Swal.fire({
        title: "Tarefa marcada como concluida!",
        timer: 3000,
        timerProgressBar: true,
        icon: "success",
      }).then(
        ListaTarefasService.listar().then((r) => {
          setItems(r.data);
          setloading(false);
        })
      );
    });
  };

  const concluirListaTarefa = (listaTarefaId, concluida) => {

    const listaTarefa = {
      listaTarefaId: listaTarefaId, Concluida: concluida
    };

    ListaTarefasService.update(listaTarefaId, listaTarefa).then((r) => {
      Swal.fire({
        title: "Lista de Tarefas marcada como concluida!",
        timer: 3000,
        timerProgressBar: true,
        icon: "success",
      }).then(
        ListaTarefasService.listar().then((r) => {
          setItems(r.data);
          setloading(false);
        })
      );
    });
  };

  const cadastro = () => {
    const listaTarefa = { nome: "", CategoriaId: 1 }

    modal.fire({
      title: "Cadastrar lista de tarefas",
      width: 800,
      html: (
        <CContainer>
          <CRow>
            <CCol sm="12">
              <CFormGroup>
                <CLabel style={{ float: "left" }}>Nome</CLabel>
                <CInput type="text" onChange={(val) => listaTarefa.nome = val.target.value} />
              </CFormGroup>
            </CCol>
          </CRow>
          <CRow>
            <CCol sm="12">
              <CFormGroup>
                <CLabel style={{ float: "left" }}>Categoria</CLabel>
                <CSelect onChange={(e) => listaTarefa.CategoriaId = parseInt(e.target.value)} >
                  <option value="1">Categoria 1</option>
                  <option value="2">Categoria 2</option>
                </CSelect>
              </CFormGroup>
            </CCol>
          </CRow>
        </CContainer>
      ),
    }).then((result) => {
      if (result.isConfirmed) {
        ListaTarefasService.salvar(listaTarefa).then((r) => {
          Swal.fire({
            title: "Lista de Tarefas salva!",
            timer: 2000,
            timerProgressBar: true,
            icon: "success",
          }).then(
            ListaTarefasService.listar().then((r) => {
              setItems(r.data);
              setloading(false);
            })
          );
        });
      }
    });;
  };

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const fields = [
    { key: "Nome", label: "Nome", _style: { width: "40%" } },
    { key: "Categoria", label: "Categoria", _style: { width: "40%" } },
    {
      key: "Concluida", label: "Concluida", _style: { width: "40%" }, filter: false,
      sorter: false
    },
    {
      key: "listaTarefasId",
      label: "",
      _style: { width: "20%" },
      filter: false,
      sorter: false,
    },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      filter: false
    }
  ];

  const fieldsTarefa = [
    { key: "Nome", label: "Nome", _style: { width: "40%" } },
    { key: "Usuario", label: "Usuario", _style: { width: "40%" } },
    {
      key: "Concluida", label: "Concluida", _style: { width: "40%" }, filter: false,
      sorter: false
    },
    {
      key: "TarefaId",
      label: "",
      _style: { width: "20%" },
      filter: false,
      sorter: false,
    },
  ];

  return (
    <>
      <CRow>
        <CCol sm="12">
          <CButton
            color="success"
            className="mr-2"
            onClick={() => cadastro()}>
            Cadastrar Lista de tarefas
        </CButton>
        </CCol>
      </CRow>
      <br />
      <CRow>
        <CCol sm="12">
          <CCard>
            <CCardHeader>
              <BsGrid3X3 /> Lista de Tarefas
          </CCardHeader>
            <CCardBody>
              <CDataTable
                items={items}
                fields={fields}
                noItemsView={{
                  noItems: "Nenhum registro para exibir",
                  noResults: "Nenhum registro para exibir",
                }}
                columnFilter
                cleaner
                itemsPerPageSelect={{ label: "Exibir", values: [10, 20, 50] }}
                itemsPerPage={10}
                //tableFilter={{ label: "Filtro", placeholder: " " }}
                hover
                sorter
                striped
                pagination
                outlined
                loading={loading}
                // onRowClick={(item,index,col,e) => console.log(item,index,col,e)}
                // onPageChange={(val) => console.log('new page:', val)}
                // onPagesChange={(val) => console.log('new pages:', val)}
                // onPaginationChange={(val) => console.log('new pagination:', val)}
                // onFilteredItemsChange={(val) => console.log('new filtered items:', val)}
                // onSorterValueChange={(val) => console.log('new sorter value:', val)}
                // onTableFilterChange={(val) => console.log('new table filter:', val)}
                // onColumnFilterChange={(val) => console.log('new column filter:', val)}
                scopedSlots={{
                  'Categoria': (item) => (
                    <td>
                      {item.Categoria.Nome}
                    </td>
                  ),
                  'Concluida': (item) => (
                    <td>
                      <CCol key={item.listaTarefasId} sm="12">
                        <div style={{ float: "left", marginLeft: "30px" }} >
                          <CInputCheckbox key={item.ListaTarefasId} defaultChecked={item.Concluida} onClick={(r) => concluirListaTarefa(item.ListaTarefasId, r.target.checked)} />
                        </div>
                      </CCol>
                    </td>
                  ),
                  'listaTarefasId': (item) => {
                    return (
                      <td>
                        <CButtonGroup key={item.ListaTarefasId}>
                          <CButton
                            className="ml-1"
                            color="secondary"
                          >
                            <FaRegEdit size={15} />
                          </CButton>
                          <CButton
                            onClick={() => excluir(item.ListaTarefasId)}
                            className="ml-1"
                            color="secondary"
                          >
                            <BsTrash size={15} />
                          </CButton>
                        </CButtonGroup>
                      </td>
                    );
                  },
                  'show_details':
                    item => {
                      return (
                        <td className="py-2">
                          <CButton
                            color="primary"
                            variant="outline"
                            shape="square"
                            size="sm"
                            onClick={() => { toggleDetails(item.listaTarefasId) }}
                          >
                            {details.includes(item.id) ? 'Esconder Tarefas' : 'Mostrar Tarefas'}
                          </CButton>
                        </td>
                      )
                    },
                  'details':
                    (item) => {
                      return (
                        <CCollapse show={details.includes(item.listaTarefasId)}>
                          <CCardBody>
                            <CRow>
                              <CCol sm="12">
                                <CButton
                                  color="success"
                                  className="mr-2"
                                //onClick={() => cadastroTarefa()}
                                  >
                                  Cadastrar tarefa
                                </CButton>
                              </CCol>
                            </CRow>
                            <br/>
                            <CDataTable
                              items={item.Tarefas}
                              fields={fieldsTarefa}
                              size="sm"
                              itemsPerPage={5}
                              pagination
                              scopedSlots={{
                                'Usuario':
                                  (item) => (
                                    <td>
                                      {item.Usuario.Nome}
                                    </td>
                                  ),
                                'Concluida': (item) => (
                                  <td>
                                    <CCol key={item.TarefasId} sm="12">
                                      <div style={{ float: "left", marginLeft: "30px" }} >
                                        <CInputCheckbox key={item.TarefaId} defaultChecked={item.Concluida} onClick={(r) => concluirTarefa(item.TarefaId, r.target.checked)} />
                                      </div>
                                    </CCol>
                                  </td>
                                ),
                                'TarefaId': (item) => {
                                  return (
                                    <td>
                                      <CButtonGroup key={item.TarefaId}>
                                        <CButton className="ml-1" color="secondary">
                                          <FaRegEdit size={15} />
                                        </CButton>
                                        <CButton
                                          onClick={() => excluirTarefa(item.TarefaId)}
                                          className="ml-1"
                                          color="secondary"
                                        >
                                          <BsTrash size={15} />
                                        </CButton>
                                      </CButtonGroup>
                                    </td>
                                  );
                                },
                              }}
                            />
                          </CCardBody>
                        </CCollapse>
                      )
                    }
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Tabela;
