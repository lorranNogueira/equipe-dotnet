﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Infra.Data.Context;

namespace TodoListProject.Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(DataBaseContext dataBaseContext) : base(dataBaseContext)
        {
        }

        public Usuario GetByNomeAndSenha(string nome, string senha)
        {
            return DataBaseContext.Usuario.Where(u => u.Nome == nome && u.Senha == senha).FirstOrDefault();
        }
    }
}
