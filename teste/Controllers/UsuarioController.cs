﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListProject.Api.ViewModels;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioAppService _usuarioAppService;
        private IMapper _mapper;

        public UsuarioController(IUsuarioAppService usuarioAppService, IMapper mapper)
        {
            _usuarioAppService = usuarioAppService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var usuarios = _usuarioAppService.GetAll();

            var ListaUsuarioViewer = _mapper.Map<IEnumerable<UsuarioViewModel>>(usuarios);

            return Ok(ListaUsuarioViewer);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var usuario = _usuarioAppService.GetById(id);

            var usuarioViewer = _mapper.Map<UsuarioViewModel>(usuario);

            return Ok(usuarioViewer);
        }

        [HttpPost]
        public void Post(UsuarioViewModel usuario)
        {
            var usuarioDominio = _mapper.Map<Usuario>(usuario);

            _usuarioAppService.Add(usuarioDominio);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var usuario = _usuarioAppService.GetById(id);
            _usuarioAppService.Remove(usuario);
        }
    }
}
