﻿
using System.Collections.Generic;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Application.Interface
{
    public interface IUsuarioAppService : IAppServiceBase<Usuario>
    {
        Usuario GetByNomeAndSenha(string nome, string senha);
    }
}
