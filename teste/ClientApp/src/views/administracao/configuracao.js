import React, { useEffect, useState } from "react";
import { alertService } from "../../app/service/alertService";
import { configuracaoService } from "../../app/service/configuracaoService";
import {
  CButton,
  CCard,
  CCardHeader,
  CCardBody,
  CCol,
  CForm,
  CInvalidFeedback,
  CFormGroup,
  CLabel,
  CInput,
  CRow,
  CTextarea,
} from "@coreui/react";

import { Formik } from "formik";
import * as Yup from "yup";
import MaskedInput, { conformToMask } from 'react-text-mask'

const cnpjMask = [ /[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/,]
const telefoneMask = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/,/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
const cepMask = [ /[0-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/ ]

const validationSchema = function (values) {
  return Yup.object().shape({
    nome: Yup.string().required("Nome é obrigatório"),
    cnpj: Yup.string(),
    logradouro: Yup.string().required("Logradouro é obrigatório"),
    estado: Yup.string().required("Estado é obrigatório"),
    municipio: Yup.string().required("municipio é obrigatório"),
    cep: Yup.string().required("CEP é obrigatório"),
    email: Yup.string().email("Email invalido").required("Email é obrigatório"),
    telefone: Yup.string().required("telefone é obrigatório"),
    observacao: Yup.string(),
  });
};

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values);
    try {
      validationSchema.validateSync(values, { abortEarly: false });
      return {};
    } catch (error) {
      return getErrorsFromValidationError(error);
    }
  };
};

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0;
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    };
  }, {});
};

const ConfiguracoesForms = () => {
  const [configuracoes, setconfiguracoes] = useState({
    nome:"",
    cnpj: "",
    logradouro: "",
    estado: "",
    municipio: "",
    cep: "",
    email: "",
    telefone: "",
    observacao: "",
  });

  useEffect(() => {

    configuracaoService.obter()
    .then((r) => {
     
     var cnpjFormatado = conformToMask(r.data.cnpj, cnpjMask).conformedValue
     var telefoneFormatado = conformToMask(r.data.telefone, telefoneMask).conformedValue
     var cepFormatado = conformToMask(r.data.endereco.cep, cepMask).conformedValue

      setconfiguracoes({
        nome: r.data.nome,
        cnpj: cnpjFormatado,
        logradouro: r.data.endereco.logradouro,
        estado: r.data.endereco.estado,
        municipio: r.data.endereco.municipio,
        cep: cepFormatado,
        email: r.data.email,
        telefone: telefoneFormatado,
        observacao: r.data.observacao,
      });

    }).catch((erro) => {
        console.log(erro);
    });
  
  }, [])


  const onSubmit = async (values, { setSubmitting, setErrors }) => {
    const configuracao = {
      id: 1,
      nome: values.nome,
      cnpj: values.cnpj.replace(/[^0-9]+/g, ''),
      endereco: {
        id: 1,
        logradouro: values.logradouro,
        estado: values.estado,
        municipio: values.municipio,
        cep: values.cep.replace(/[^0-9]+/g, ''),
      },
      email: values.email,
      telefone: values.telefone.replace(/[^0-9]+/g, ''),
      observacao: values.observacao,
    };

    configuracaoService.salvar(configuracao)
      .then((r) => {
        alertService.success(["Configuração salva com sucesso!"], {
          autoClose: true,
          keepAfterRouteChange: false,
        });
        setSubmitting(false);
      }).catch((erro) => {
          console.log(erro);
      });

  };

  return (
    <CCard>
      <CCardBody>
        <Formik
          initialValues={configuracoes}
          validate={validate(validationSchema)}
          onSubmit={onSubmit}
          enableReinitialize={true}
        >
          {({
            values,
            errors,
            touched,
            status,
            dirty,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            setTouched,
          }) => (
            <CForm onSubmit={handleSubmit} noValidate name="form">
              <CRow>
                <CCol lg="6">
                  <CCardHeader>Dados da pousada</CCardHeader>
                  <br />
                  <CFormGroup>
                    <CLabel htmlFor="nome">Nome</CLabel>
                    <CInput
                      maxLength={30}
                      type="text"
                      name="nome"
                      id="nome"
                      placeholder="Nome"
                      autoComplete="nome"
                      valid={!errors.nome}
                      invalid={touched.nome && !!errors.nome}
                      autoFocus={true}
                      required
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.nome}
                    />
                    <CInvalidFeedback>{errors.nome}</CInvalidFeedback>
                  </CFormGroup>
                  <CRow>
                    <CCol md={7}>
                      <CFormGroup>
                        <CLabel htmlFor="cnpj">CNPJ</CLabel>
                        <MaskedInput
                          mask={cnpjMask}
                          type="text"
                          name="cnpj"
                          id="cnpj"
                          placeholder="CNPJ"
                          autoComplete="family-name"
                          valid={!errors.cnpj}
                          invalid={touched.cnpj && !!errors.cnpj}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.cnpj}
                          render={(ref, props) => (
                            <CInput innerRef={ref} {...props}/>
                          )}
                        />
                        <CInvalidFeedback>{errors.cnpj}</CInvalidFeedback>
                      </CFormGroup>
                    </CCol>
                    <CCol md={5}>
                      <CFormGroup>
                        <CLabel htmlFor="telefone">Telefone</CLabel>
                        <MaskedInput
                          mask={telefoneMask}
                          type="text"
                          name="telefone"
                          id="telefone"
                          placeholder="Telefone"
                          autoComplete="telefone"
                          valid={!errors.telefone}
                          invalid={touched.telefone && !!errors.telefone}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.telefone}
                          render={(ref, props) => (
                            <CInput innerRef={ref} {...props}/>
                          )}
                        />
                        <CInvalidFeedback>{errors.telefone}</CInvalidFeedback>
                      </CFormGroup>
                    </CCol>
                  </CRow>
                  <CFormGroup>
                    <CLabel htmlFor="email">Email</CLabel>
                    <CInput
                      maxLength={50}
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Email"
                      autoComplete="email"
                      valid={!errors.email}
                      invalid={touched.email && !!errors.email}
                      required
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                    />
                    <CInvalidFeedback>{errors.email}</CInvalidFeedback>
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="observacao">observação</CLabel>
                    <CTextarea
                      maxLength={400}
                      type="text"
                      name="observacao"
                      id="observacao"
                      placeholder="observação"
                      autoComplete="observacao"
                      valid={!errors.observacao}
                      invalid={touched.observacao && !!errors.observacao}
                      required
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.observacao}
                    />
                    <CInvalidFeedback>{errors.observacao}</CInvalidFeedback>
                  </CFormGroup>
                </CCol>
                <CCol lg="6">
                  <CCardHeader>Endereço da pousada</CCardHeader>
                  <br />
                  <CFormGroup>
                    <CLabel htmlFor="logradouro">Logradouro</CLabel>
                    <CInput
                      maxLength={80}
                      type="text"
                      name="logradouro"
                      id="logradouro"
                      placeholder="logradouro"
                      autoComplete="logradouro"
                      valid={!errors.logradouro}
                      invalid={touched.logradouro && !!errors.logradouro}
                      required
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.logradouro}
                    />
                    <CInvalidFeedback>{errors.logradouro}</CInvalidFeedback>
                  </CFormGroup>
                  <CFormGroup>
                        <CLabel htmlFor="municipio">Município</CLabel>
                        <CInput
                          maxLength={50}
                          type="text"
                          name="municipio"
                          id="municipio"
                          placeholder="municipio"
                          autoComplete="municipio"
                          valid={!errors.municipio}
                          invalid={touched.municipio && !!errors.municipio}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.municipio}
                        />
                        <CInvalidFeedback>{errors.municipio}</CInvalidFeedback>
                    </CFormGroup>
                  <CRow>
                    <CCol md={6}>
                      <CFormGroup>
                        <CLabel htmlFor="estado">Estado</CLabel>
                        <CInput
                          maxLength={30}
                          type="text"
                          name="estado"
                          id="estado"
                          placeholder="estado"
                          autoComplete="estado"
                          valid={!errors.estado}
                          invalid={touched.estado && !!errors.estado}
                          required
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.estado}
                        />
                        <CInvalidFeedback>{errors.estado}</CInvalidFeedback>
                      </CFormGroup>
                    </CCol>
                    <CCol md={6}>
                      <CFormGroup>
                        <CLabel htmlFor="cep">CEP</CLabel>
                        <MaskedInput
                              mask={cepMask}
                              type="text"
                              name="cep"
                              id="cep"
                              placeholder="CEP"
                              autoComplete="cep"
                              valid={!errors.cep}
                              invalid={touched.cep && !!errors.cep}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.cep}
                              render={(ref, props) => (
                                <CInput innerRef={ref} {...props}/>
                              )}
                            />
                        <CInvalidFeedback>{errors.cep}</CInvalidFeedback>
                      </CFormGroup>
                    </CCol>
                  </CRow>               
                </CCol>
                <CCol>
                  <br />
                  <CFormGroup>
                    <CButton
                      type="submit"
                      color="success"
                      className="mr-1"
                      disabled={isSubmitting}
                      //onClick={() => touchAll(setTouched, errors)}
                    >
                      {isSubmitting ? "Salvando..." : "Salvar"}
                    </CButton>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CForm>
          )}
        </Formik>
      </CCardBody>
    </CCard>
  );
};

export default ConfiguracoesForms;
