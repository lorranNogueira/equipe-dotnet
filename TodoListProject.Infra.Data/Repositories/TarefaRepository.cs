﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Infra.Data.Context;

namespace TodoListProject.Infra.Data.Repositories
{
    public class TarefaRepository : RepositoryBase<Tarefa>, ITarefaRepository
    {
        public TarefaRepository(DataBaseContext dataBaseContext) : base(dataBaseContext)
        {
        }

        public IEnumerable<Tarefa> GetByIdListaTarefas(int ListaId)
        {
            return DataBaseContext.Tarefa.Where(t => t.ListaTarefas.ListaTarefasId == ListaId);
        }
    }
}
