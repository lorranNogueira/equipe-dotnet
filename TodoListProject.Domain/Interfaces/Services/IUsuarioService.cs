﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Domain.Interfaces.Services
{
    public interface IUsuarioService : IServiceBase<Usuario>
    {
        Usuario GetByNomeAndSenha(string nome, string senha);
    }
}
