import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  CCard,
  CCardHeader,
  CCol,
  CRow,
  CCardBody,
  CBadge,
  CButton,
  CButtonGroup,
  CDataTable,
  CLabel,
  CInput,
  CInputCheckbox,
  CContainer,
  CFormGroup,
} from "@coreui/react";
import { BsEye, BsTrash, BsGrid3X3 } from "react-icons/bs";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import UsuarioService from "../../app/service/usuarioService";

const modal = withReactContent(Swal);

const Tabela = () => {
  useEffect(() => {
    UsuarioService.listarUsuarios().then((r) => {
      setItems(r.data);
      setloading(false);
    });
  }, []);

  const [items, setItems] = useState();
  const [loading, setloading] = useState(true);
  let history = useHistory();

  const excluir = (id) => {
    Swal.fire({
      title: "Excluir?",
      text: "Não vai ser possível desfazer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "Cancelar",
      confirmButtonText: "Sim, excluir!",
    }).then((result) => {
      if (result.isConfirmed) {
        UsuarioService.removerUsuario(id).then((r) => {
          Swal.fire({
            title: "Usuario excluído!",
            timer: 2000,
            timerProgressBar: true,
            icon: "success",
          }).then(
            UsuarioService.listarUsuarios().then((r) => {
              setItems(r.data);
              setloading(false);
            })

          );
        });
      }
    });
  };

  const cadastro = () => {
    history.push("/usuario/cadastro/");
  };

  const vizualizar = (id) => {
    modal.fire({
      title: "Carregando dados do Usuário",
      showCloseButton: true,
      focusConfirm: true,
      width: 300,
      didOpen: () => {
        modal.showLoading();
        UsuarioService.obterUsuario(id).then((r) => {
          modal.hideLoading();
          modal.fire({
            title: "Dados do Usuário",
            width: 800,
            html: (
              <CContainer>
                <CRow>
                  <CCol sm="12">
                    <CFormGroup>
                      <CLabel style={{ float: "left" }}>Nome</CLabel>
                      <CInput type="text" value={r.data.Nome} disabled />
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="12">
                    <CFormGroup>
                      <CLabel style={{ float: "left" }}>Email</CLabel>
                      <CInput type="text" value={r.data.Email} disabled />
                    </CFormGroup>
                  </CCol>
                </CRow>
              </CContainer>
            ),
          });
        });
      },
    });
  };

  const fields = [
    { key: "Nome", label: "Nome", _style: { width: "40%" } },
    { key: "Email", label: "Email", _style: { width: "40%" } },
    {
      key: "usuarioId",
      label: "",
      _style: { width: "20%" },
      filter: false,
      sorter: false,
    },
  ];

  return (
    <>
      <CRow>
        <CCol sm="12">
          <CButton
            color="success"
            className="mr-2"
            onClick={() => cadastro()}>
            Cadastrar Usuário
        </CButton>
        </CCol>
      </CRow>
      <br />
      <CRow>
        <CCol sm="12">
          <CCard>
            <CCardHeader>
              <BsGrid3X3 /> Lista de Usuários de sistema
          </CCardHeader>
            <CCardBody>
              <CDataTable
                items={items}
                fields={fields}
                noItemsView={{
                  noItems: "Nenhum registro para exibir",
                  noResults: "Nenhum registro para exibir",
                }}
                cleaner
                itemsPerPageSelect={{ label: "Exibir", values: [10, 20, 50] }}
                itemsPerPage={10}
                tableFilter={{ label: "Filtro", placeholder: " " }}
                hover
                sorter
                striped
                pagination
                outlined
                loading={loading}
                // onRowClick={(item,index,col,e) => console.log(item,index,col,e)}
                // onPageChange={(val) => console.log('new page:', val)}
                // onPagesChange={(val) => console.log('new pages:', val)}
                // onPaginationChange={(val) => console.log('new pagination:', val)}
                // onFilteredItemsChange={(val) => console.log('new filtered items:', val)}
                // onSorterValueChange={(val) => console.log('new sorter value:', val)}
                // onTableFilterChange={(val) => console.log('new table filter:', val)}
                // onColumnFilterChange={(val) => console.log('new column filter:', val)}
                scopedSlots={{
                  usuarioId: (item) => {
                    return (
                      <td>
                        <CButtonGroup key={item.UsuarioId}>
                          <CButton
                            className="ml-1"
                            onClick={() => vizualizar(item.UsuarioId)}
                            color="secondary"
                          >
                            <BsEye size={15} />
                          </CButton>
                          <CButton
                            onClick={() => excluir(item.UsuarioId)}
                            className="ml-1"
                            color="secondary"
                          >
                            <BsTrash size={15} />
                          </CButton>
                        </CButtonGroup>
                      </td>
                    );
                  },
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Tabela;
