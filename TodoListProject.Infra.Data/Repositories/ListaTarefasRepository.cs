﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Infra.Data.Context;

namespace TodoListProject.Infra.Data.Repositories
{
    public class ListaTarefasRepository : RepositoryBase<ListaTarefas>, IListaTarefasRepository
    {
        public ListaTarefasRepository(DataBaseContext dataBaseContext) : base(dataBaseContext)
        {
        }

        public ListaTarefas GetListaTarefa(int listaTarefaId)
        {
            return DataBaseContext.ListaTarefas
                    .Where(t => t.ListaTarefasId == listaTarefaId)
                    .Include(t => t.Categoria)
                    .Include(t => t.Tarefas)
                    .SingleOrDefault();
        }

        public IEnumerable<ListaTarefas> GetAllListaTarefa()
        {
            return DataBaseContext.ListaTarefas
                    .Include(t => t.Categoria)
                    .Include(t => t.Tarefas)
                    .ThenInclude(u => u.Usuario)
                    .ToList();
        }
    }
}
