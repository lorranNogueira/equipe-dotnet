﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Api.ViewModels
{
    public class ListaTarefasViewModel
    {
        [Key]
        public long ListaTarefasId { get; set; }
        public string Nome { get; set; }
        public long CategoriaId { get; set; }
        public virtual CategoriaViewModel Categoria { get; set; }
        public bool Concluida { get; set; }
        public virtual ICollection<TarefaViewModel> Tarefas { get; set; }
    }
}
