Fiz o backend da aplicação usando a arquitetura DDD com C# ASP.NET core 3.1, Entity Framework Core 5 com banco de dados em memoria. 

No frontend utilizei React hooks, aproveitei um template de um projeto pessoal que eu estou desenvolvendo.

Tive dificuldade para configurar o in memory database entity framework core e configurar a injeção de dependência.

Como já tinha implementando previamente, fiz a listagem e o excluir de usuario.

infelizmente por conta do tempo, fiz testes básicos nas funcionalidades e implementei as seguintes funcionalidades:

### Lista de tarefas

- Criar lista (Parcialmente feita)
- Editar

### Tarefa

- Adicionar
- Editar

Acredito que se tivesse mais algumas horas conseguiria terminar e implementar outras funcionalidades com JWT.

Esse foi o meu primeiro projeto utilizando ASP.NET core 3.1, Entity Framework Core.

PS.: O vídeo tutorial do Eduardo Pires é muito bom, já fiz um curso presencial ministrado por ele. 