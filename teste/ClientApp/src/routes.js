import React from 'react'
import UserService from "./app/service/userService";

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const Agenda = React.lazy(() => import('./views/agenda/Agenda'))
const Configuracao = React.lazy(() => import('./views/administracao/configuracao'))
const ListaUsuario = React.lazy(() => import('./views/usuario/lista'))
const CadastroUsuario = React.lazy(() => import('./views/usuario/cadastro'))
const ListaTarefas = React.lazy(() => import('./views/listaTarefas/lista'))


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', recurso: 'ROLE_VISUALIZAR_DASHBOARD', name: 'Dashboard', component: Dashboard },
  { path: '/agenda', recurso: 'ROLE_VISUALIZAR_AGENDA', name: 'Agenda', component: Agenda },
  { path: '/administracao/configuracao', recurso: 'ROLE_CONFIGURACOES_POUSADA', name: 'Configuracao', component: Configuracao },
  { path: '/usuario/lista', recurso: 'ROLE_USUARIO_LISTAR', name: 'UsuarioLista', component: ListaUsuario },
  { path: '/usuario/cadastro', recurso: 'ROLE_USUARIO_LISTAR', name: 'UsuarioCadastro', component: CadastroUsuario },
  { path: '/listaTarefas/lista', component: ListaTarefas },
]

function rotasAutorizadas (){
  //var recursos = UserService.recursos();
  //const rotas = routes.filter(r => recursos.some(n => n === r.recurso))
  return routes
}

export default rotasAutorizadas()
