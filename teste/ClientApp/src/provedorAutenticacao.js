import React, { useState, createContext, useEffect, useContext } from "react";

// Create Context Object
export const UsuarioContext = createContext({});
const KEY = "USER";

// Create a provider for components to consume and subscribe to changes
export const UsuarioContextProvider = (props) => {
  const [usuario, setUsuario] = useState(JSON.parse(localStorage.getItem(KEY)));

  useEffect(() => {
    const storagedUser = localStorage.getItem(KEY);

    if (storagedUser) {
        const user = JSON.parse(storagedUser);
        setUsuario(user);
    }
  }, []);

  function iniciarSessao(usuario){
    localStorage.setItem(KEY, JSON.stringify(usuario));
    setUsuario(usuario);
  };

  const encerrarSessao = (usuario) => {
    localStorage.removeItem(KEY);
    setUsuario(null);
  };

  return (
    <UsuarioContext.Provider value={{
        iniciarSessao: iniciarSessao,
        isAutenticado: Boolean(usuario),
        usuario: usuario,
        encerrarSessao: encerrarSessao
        }}>
      {props.children}
    </UsuarioContext.Provider>
  );
};

export function useAuth() {
    const context = useContext(UsuarioContext);
    return context;
  }