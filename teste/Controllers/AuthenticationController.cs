﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoListProject.Api.ViewModels;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Interfaces.Repositories;

namespace TodoListProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {

        private readonly IUsuarioAppService _usuarioAppService;
        private IMapper _mapper;

        public AuthenticationController(IUsuarioAppService usuarioAppService, IMapper mapper)
        {
            _usuarioAppService = usuarioAppService;
            _mapper = mapper;
        }

        // POST api/<ValuesController>
        [HttpPost]
        public IActionResult Authenticate(UsuarioAuthenticationViewModel usuarioAuthentication)
        {
            var result = _usuarioAppService.GetByNomeAndSenha(usuarioAuthentication.nome, usuarioAuthentication.senha);
            if (result == null)
            {
                return NotFound();
            }

           var usuarioViewer = _mapper.Map<UsuarioViewModel>(result);

            return Ok(usuarioViewer);
        }
    }
}
