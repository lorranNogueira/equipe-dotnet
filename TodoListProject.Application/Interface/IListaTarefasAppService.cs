﻿
using System.Collections.Generic;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Application.Interface
{
    public interface IListaTarefasAppService : IAppServiceBase<ListaTarefas>
    {
        ListaTarefas GetListaTarefa(int listaTarefaId);
        IEnumerable<ListaTarefas> GetAllListaTarefa();
    }
}
