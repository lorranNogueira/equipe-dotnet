﻿using System;
using System.Collections.Generic;
using System.Text;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Domain.Interfaces.Repositories
{
    public interface ITarefaRepository : IRepositoryBase<Tarefa>
    {
        IEnumerable<Tarefa> GetByIdListaTarefas(int ListaId);
    }
}
