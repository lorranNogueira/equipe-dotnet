﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoListProject.Api.ViewModels
{
    public class CategoriaViewModel
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }
    }
}
