﻿using Microsoft.EntityFrameworkCore;
using TodoListProject.Domain.Entities;

namespace TodoListProject.Infra.Data.Context
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<ListaTarefas> ListaTarefas { get; set; }
        public DbSet<Tarefa> Tarefa { get; set; }
        public DbSet<Categoria> Categoria { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Tarefa>().HasKey(x => new { x.ListaTarefasId});

            //modelBuilder.Entity<ListaTarefas>()
            //     .HasMany(e => e.Tarefas)
            //     .WithOne(e => e.ListaTarefas)
            //     .HasForeignKey(f => f.ListaTarefasId)
            //     .OnDelete(DeleteBehavior.Cascade);



            modelBuilder.Entity<Tarefa>()
                   .HasOne<ListaTarefas>(l => l.ListaTarefas)
                   .WithMany(e => e.Tarefas)
                   .HasForeignKey(f => f.ListaTarefasId)
                   .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ListaTarefas>()
                  .HasOne(c => c.Categoria);

            modelBuilder.Entity<Tarefa>()
                  .HasOne(c => c.Usuario);


        }
    }

}

