﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoListProject.Api.ViewModels
{
    public class UsuarioAuthenticationViewModel
    {
        [Required(ErrorMessage = "preencha o campo Login")]
        public string nome { get; set; }

        [Required(ErrorMessage = "preencha o campo Senha")]
        public string senha { get; set; }
    }
}
