﻿

using System.Collections.Generic;
using System.Linq;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Domain.Interfaces.Services;

namespace TodoListProject.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
            : base(usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public Usuario GetByNomeAndSenha(string nome, string senha)
        {
            return _usuarioRepository.GetByNomeAndSenha(nome, senha);
        }

    }
}
