import axios from "axios";
import userService from "../app/service/userService";
import Swal from "sweetalert2";

let api = axios.create({
  baseURL: "http://localhost:54493/api",
});

api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response) {

      const mensagens = () =>{
       return error.response.data.errors.map((mensagem, index) => '<div>' + mensagem + '</div>')
      }

      if (error.response.status === 400) {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          html: mensagens(),
          showConfirmButton: false,
          timer: 6000
        })

      } else if (error.response.status === 401) {
        Swal.fire({
          icon: "error",
          title: "Sessão expirada",
          text:
            "Sua sessão expirou. Você gostaria de ser redirecionado para a página de login?",
          closeOnConfirm: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Sim",
          showCancelButton: true,
          CancelButtonText: "Cancelar",
        }).then((result) => {
          if (result.isConfirmed) {
            localStorage.removeItem("USER");
            window.location = "/login";
          }
        });
      } else {
        
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Sistema indisponível ou falha na conexão',
          showConfirmButton: false,
          timer: 3000
        })



      }
    } else {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Sistema indisponível ou falha na conexão',
        showConfirmButton: false,
        timer: 3000
      })
    }

    return Promise.reject(error);
  }
);

const update = (url, objeto) => {
  //const hash = userService.obterHashAutenticacao();

  return api.put(url, objeto, {
    //headers: {
    //  Authorization: hash,
    //},
  });
};

const remove = (url) => {
  //const hash = userService.obterHashAutenticacao();

  return api.delete(url, {
    //headers: {
    //  Authorization: hash,
    //},
  });
};

const post = (url, objeto) => {
 // const hash = userService.obterHashAutenticacao();

  return api.post(url, objeto, {
    //headers: {
    //  Authorization: hash,
    //},
  });
};

const get = (url) => {
  //const hash = userService.obterHashAutenticacao();

  return api.get(url, {
    //headers: {
    //  Authorization: hash,
    //},
  });
};

export default {
  api,
  post,
  get,
  remove,
  update
};

// class ApiService {

//     post(url, objeto){
//         const requestUrl = `${this.apiurl}${url}`
//         return httpClient.post(requestUrl, objeto);
//     }

//     put(url, objeto){
//         const requestUrl = `${this.apiurl}${url}`
//         return httpClient.put(requestUrl, objeto);
//     }

//     delete(url){
//         const requestUrl = `${this.apiurl}${url}`
//         return httpClient.delete(requestUrl)
//     }
