﻿

using System.Collections.Generic;
using System.Linq;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Domain.Interfaces.Services;


namespace TodoListProject.Domain.Services
{
    public class TarefaService : ServiceBase<Tarefa>, ITarefaService
    {
        private readonly ITarefaRepository _tarefaRepository;

        public TarefaService(ITarefaRepository tarefaRepository)
            : base(tarefaRepository)
        {
            _tarefaRepository = tarefaRepository;
        }
    }
}
