﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoListProject.Domain.Entities
{
    public class Tarefa
    {
        public int TarefaId { get; set; }
        public int ListaTarefasId { get; set; }
        public virtual ListaTarefas ListaTarefas { get; set; }
        public int UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
        public string Nome { get; set; }
        public bool Concluida { get; set; }
    }
}
