﻿

using System.Collections.Generic;
using TodoListProject.Application;
using TodoListProject.Application.Interface;
using TodoListProject.Domain.Entities;
using TodoListProject.Domain.Interfaces.Services;

namespace TodoListProject.Application
{
    public class TarefaAppService : AppServiceBase<Tarefa>, ITarefaAppService
    {
        private readonly ITarefaService _tarefaService;

        public TarefaAppService(ITarefaService tarefaService)
            : base(tarefaService)
        {
            _tarefaService = tarefaService;
        }
    }
}
