﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoListProject.Domain.Interfaces.Repositories;
using TodoListProject.Infra.Data.Context;

namespace TodoListProject.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected DataBaseContext DataBaseContext;

        public RepositoryBase(DataBaseContext dataBaseContext)
        {
            DataBaseContext = dataBaseContext;
        }

        public void Add(TEntity obj)
        {
            DataBaseContext.Set<TEntity>().Add(obj);
            DataBaseContext.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return DataBaseContext.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DataBaseContext.Set<TEntity>().ToList();
        }

        public void Update(TEntity obj)
        {
            DataBaseContext.Entry(obj).State = EntityState.Modified;
            DataBaseContext.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            DataBaseContext.Set<TEntity>().Remove(obj);
            DataBaseContext.SaveChanges();
        }

        public void Dispose()
        {
           // throw new NotImplementedException();
        }
    }
}
